package com.company;

import static org.junit.Assert.*;

public class MyClassTest {
    private MyClass testClass;

    @org.junit.Before
    public void setUp() throws Exception {
        testClass = new MyClass();
    }

    @org.junit.Test
    public void sayHello() throws Exception {
        assertEquals("Hello",testClass.sayHello());
    }

}